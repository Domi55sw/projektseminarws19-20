#!python3
#http://www.steves-internet-guide.com/into-mqtt-python-client/
#http://www.steves-internet-guide.com/client-connections-python-mqtt/
​
import paho.mqtt.client as mqtt
import time
​
class mqttMethods:
    # initialisiere Client instanz
    def __init__(self, clientname):
        self.clientname = clientname
        mqtt.Client.connected_flag = False #create flag in class
        self.client = mqtt.Client(clientname)
​
    def on_connect(self, userdata, flags, rc):
        if rc == 0:
            self.client.connected_flag = True  # set flag
            print("connected OK Returned code=", rc)
        else:
            print("Bad connection Returned code=", rc)
            self.client.bad_connection_flag=True
​
    def SimpleConnectBroker(self, on_connect, broker_address):
        self.client.on_connect = on_connect
        self.client.loop_start()
        self.client.connect(broker_address)   #connect to broker
        while not self.client.connected_flag and not self.client.bad_connection_flag: #wait in loop
            print("In wait loop")
            time.sleep(1)
        if self.client.bad_connection_flag:
            self.client.loop_stop()  # Stop loop
            self.sys.exit()
        print("in Main Loop")
        self.client.loop_stop()    #Stop loop
​
    def AuthConnectBroker(self,username,password,broker_address):
        self.client.username_pw_set(username=username,password=password)
        self.SimpleConnectBroker(broker_address)
​
#callback functions prints received messages
    def on_message(client, userdata, message):
        print("message received ", str(message.payload.decode("utf-8")))
        print("message topic=", message.topic)
        print("message qos=", message.qos)
        print("message retain flag=", message.retain)
​
​
    def SubscribeTopic(self,topic,payload):
        self.client.on_message = self.on_message # attach function to callback
        self.client.subscribe(topic,qos=0)